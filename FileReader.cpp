//Incldue Directives
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

//Function Name: readSpecimenFile()
//Function Description: Takes a file name as input and reads file contents before returning
//a vector string of file contents.
vector<string> readSpecimenFile(string fileName)
{
    //Variable declarations
    ifstream fileReaderObject;
    vector<string> specimenFileContents;
    string currentLine = "";

    //Attempt to load the specimen file
    fileReaderObject.open(fileName);
    if (fileReaderObject.fail())
    {
        cout << "Unable to continue, it looks like an error occurred." << endl;
        cout << "The specimen file you provided was not readable.  Please" << endl;
        cout << "double check the file name and try again." << endl;
        cout << endl << endl;
        cout << "QuickSand is exiting..." << endl;
        exit(EXIT_FAILURE);
    }

    //Attempt to iterate through the file
    if (fileReaderObject.is_open())
    {
        while (getline(fileReaderObject, currentLine))
        {
            specimenFileContents.push_back(currentLine);
        }
    }

    //Return specimenFileContents on completion
    return specimenFileContents;
}