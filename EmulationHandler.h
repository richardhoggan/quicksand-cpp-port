#include <string>
#include <vector>
#include "EmulationOutput.h"

using namespace std;

EmulationOutput emulationHandler(vector<vector<string>> pushStack, vector<vector<string>> instructionStack);