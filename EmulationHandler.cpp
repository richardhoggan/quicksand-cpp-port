#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include <sstream>
#include <map>

#include "EmulationOutput.h"

using namespace std;

EmulationOutput emulationHandler(vector<vector<string>> pushStack, vector<vector<string>> instructionStack)
{
    //Variable declarations
    bool executionCompleted = false;
    int pushStackLength = 0;
    int instructionStackLength = 0;
    stack<string> dataStack;
    string eaxRegisterValue = "";
    unsigned int eaxRegisterValueAsInteger = 0;
    EmulationOutput emulatedOutputReturnObject;

    map<string, unsigned int> emulatedMainMemoryMap;
    map<string, unsigned int> sandboxedMainMemoryMap;

    //vector<int> emulatedMainMemory;
    //vector<int> sandboxedMainMemory;
    bool xorSelfFlag = false;

    //Validate function arguments before continuing
    pushStackLength = pushStack.size();
    instructionStackLength = instructionStack.size();
    if (pushStackLength > 0 && instructionStackLength > 0)
    {
        //Generate a stack with all pushed data
        cout << "Generating data stack..." << endl;
        for (int currentStackPosition = 0; currentStackPosition < pushStackLength; currentStackPosition++)
        {
            //Get the current push instruction and parse out value
            vector<string> currentPushInstruction = pushStack[currentStackPosition];
            string currentPushValue = currentPushInstruction[1];

            //Determine if the current push value is 0
            //Pass on 0
            if (currentPushValue != "0x0" || currentPushValue != "0x0")
            {
                dataStack.push(currentPushValue);
                cout << "Current value being pushed-> " << currentPushValue << endl;
            }
            else { continue; }
        }
        cout << "Data stack generation completed..." << endl << endl;

        //Iterate through instruction stack and begin emulation
        cout << "Executing instructions..." << endl << endl;
        for (int currentInstructionStackPosition = 0;
             currentInstructionStackPosition < instructionStackLength; currentInstructionStackPosition++)
        {
            //Get the current instruction
            vector<string> currentInstruction = instructionStack[currentInstructionStackPosition];
            string currentOpCode = currentInstruction[0];

            //Expected Opcodes
            //pop - this will pop a command off the stack
            //xor - this will conduct a logical XOR operation against the left and right operands
            //mov - this will move the operand value to the data memory

            //Determine the current opcode
            if (currentOpCode == "pop")
            {
                cout << "Current Instruction Executing->" << currentInstruction[0] << " "
                     << currentInstruction[1] << endl;

                //If the current opcode is pop then pop off the data stack
                //and store in the destination register
                string destinationOperand = currentInstruction[1];
                if (destinationOperand == "eax")
                {
                    eaxRegisterValue = dataStack.top();
                }
                cout << "Value popped off the stack->" << dataStack.top() << endl << endl;
                dataStack.pop();
            }
            else if (currentOpCode == "xor")
            {
                cout << "Current Instruction Execution->" << currentInstruction[0] << " " << currentInstruction[1]
                     << " " << currentInstruction[2] << endl;

                //If the current opcode is xor then XOR the operands together
                //On completion put back in EAX register
                unsigned int leftOperand;
                unsigned int rightOperand;
                stringstream leftOperandToHex;
                stringstream rightOperandToHex;
                unsigned int xorResult;

                //Determine if the left operand is a register value
                if (currentInstruction[1] == "eax")
                {
                    //Send the current eax value into the left operand and convert to hex
                    leftOperandToHex << hex << eaxRegisterValue;
                    leftOperandToHex >> leftOperand;
                }
                else
                {
                    //Send the current value in index position 1 and convert to hex
                    leftOperandToHex << hex << currentInstruction[1];
                    leftOperandToHex >> leftOperand;
                }

                //Determine if the right operand is a register value
                if (currentInstruction[2] == "eax")
                {
                    //Send the current eax value into the right operand and convert to hex
                    rightOperandToHex << hex << eaxRegisterValue;
                    rightOperandToHex >> rightOperand;
                }
                else
                {
                    //Send the current value in index position 1 and convert to hex
                    rightOperandToHex << hex << currentInstruction[2];
                    rightOperandToHex >> rightOperand;
                }

                if (currentInstruction[1] == "eax" and currentInstruction[2] == "eax")
                {
                    cout << "A potentially malicious indicator has occurred..." << endl;
                    cout << "Setting the xorSelfFlag..." << endl;
                    xorSelfFlag = true;
                }

                //Conduct XOR
                eaxRegisterValueAsInteger = leftOperand ^ rightOperand;
                cout << "XOR'ed Value->" << eaxRegisterValueAsInteger << endl << endl;
            }
            else if (currentOpCode == "mov")
            {
                cout << "Current Instruction Execution->" << currentInstruction[0]
                     << " " << currentInstruction[1] << currentInstruction[2] << endl;

                //If the current opcode is mov - then check if the current source register is the eax register
                //mov instruction format: mov [0], eax where the register format is destination,source
                string destinationRegister = currentInstruction[1];
                string sourceRegister = currentInstruction[2];
                if (sourceRegister == "eax")
                {
                    //Before continuing with instruction execution - determine if the xorSelfFlag is set
                    if (!xorSelfFlag)
                    {
                        //If the source register is eax then map to the specified memory location
                        //in the emulatedMainMemoryMap
                        emulatedMainMemoryMap[destinationRegister] = eaxRegisterValueAsInteger;
                        cout << endl;
                    }
                    else
                    {
                        //If the xorSelfFlag is set then sandbox the main memory in order to preserve xored values
                        cout << "Moving current eax register value to sandboxed memory..." << endl;
                        cout << "At location:" << destinationRegister << endl;
                        cout << "Current eax register value:" << eaxRegisterValueAsInteger << endl << endl;
                        sandboxedMainMemoryMap[destinationRegister] = eaxRegisterValueAsInteger;
                    }
                }
            }
            cout << endl << "Instruction execution emulation completed..." << endl;
        }

        //Build the final return object and return from the emulationHandler on completion
        emulatedOutputReturnObject.emulatedMainMemoryMap = emulatedMainMemoryMap;
        emulatedOutputReturnObject.sandboxedEmulatedMainMemoryMap = sandboxedMainMemoryMap;
    }

    return emulatedOutputReturnObject;
}