#include <vector>
#include <string>
using namespace std;

vector<vector<string>> tokenizeSpecimenFileContents(vector<string> specimenFileContents);
vector<vector<string>> parseAsStack(vector<vector<string>> tokenizedSpecimenFileContents, string parsingType);