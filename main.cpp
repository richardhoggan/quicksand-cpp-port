//Program: QuickSand
//Developer: Rich Hoggan
//Created On: 04/03/2017
//Description: QuickSand is a sand boxing application for parsing through obfuscated
//assembly level code and executing said code in order to determine a
//decryption key.  Further, if the application detects that the specimen program is trying to
//mask decoded memory (or attempt to delete) decoded memory, the application will sandbox
//the remaining execution and preserve the decoded memory values.

//Include directives
#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <map>

#include "FileReader.h"
#include "SequenceParsing.h"
#include "EmulationHandler.h"
#include "EmulationOutput.h"

//Namespace directives
using namespace std;

//Function prototypes
void printProgramInformationHeader();
void printUsageStatement();
void printErrorMessage(string errorTypeFlag);

//Application code
int main(int argc, char *argv[])
{
    //Variable declarations
    int totalCommandLineArguments = 0;
    string specimenFileName = "";
    vector<string> specimenFileContents;
    vector<vector<string>> tokenizedSpecimenFileContents;
    vector<vector<string>> tokenizedPushStack;
    vector<vector<string>> tokenizedInstStack;
    EmulationOutput emulatedOutputObject;

    //Get command line arguments and validate for correctness
    totalCommandLineArguments = argc;
    if (totalCommandLineArguments != 2)
    {
        //If the total number of command line arguments is invalid then
        //print the usage statement and exit the application
        printUsageStatement();
        exit(EXIT_FAILURE);
    }
    else if (totalCommandLineArguments == 2)
    {
        //Get the specimen file name from the command line arguments
        //and attempt to read in file contents
        specimenFileName = argv[1];
        specimenFileContents = readSpecimenFile(specimenFileName);

        //Parse the specimen file contents into readable form
        tokenizedSpecimenFileContents = tokenizeSpecimenFileContents(specimenFileContents);
        tokenizedPushStack = parseAsStack(tokenizedSpecimenFileContents, "PUSH_STACK");
        tokenizedInstStack = parseAsStack(tokenizedSpecimenFileContents, "INST_STACK");

        emulatedOutputObject = emulationHandler(tokenizedPushStack, tokenizedInstStack);

        //Begin analysis of the emulated assembly
        map<string, unsigned int> emulatedMainMemory = emulatedOutputObject.emulatedMainMemoryMap;
        cout << "Captured Main Memory:" << endl;
        for (map<string, unsigned int>::iterator positionIncrementer = emulatedMainMemory.begin();
                positionIncrementer != emulatedMainMemory.end(); positionIncrementer++)
        {
            cout << "Memory Location>" << positionIncrementer->first << endl;
            cout << "Memory Value>" << positionIncrementer->second << endl;

            //Determine what type of value is stored in memory
            if (positionIncrementer->second >= 0 && positionIncrementer->second <= 127)
            {
                cout << "Potential ASCII Value>" << (char)positionIncrementer->second << endl;
            }
        }
        cout << endl;
        cout << "Sandboxed Main Memory:" << endl;
        map<string, unsigned int> sandboxedMainMemory = emulatedOutputObject.sandboxedEmulatedMainMemoryMap;
        for (map<string, unsigned int>::iterator positionIncrementer = sandboxedMainMemory.begin();
             positionIncrementer != sandboxedMainMemory.end(); positionIncrementer++)
        {
            cout << "Memory Location:" << positionIncrementer->first << endl;
            cout << "Memory Value:" << positionIncrementer->second << endl;
        }
    }

    return 0;
}

//Function Code
void printProgramInformationHeader()
{
    cout << "QuickSand.cpp" << endl;
    cout << "Developed By: Rich Hoggan" << endl;
}

void printUsageStatement()
{
    cout << "QuickSand.cpp" << endl;
    cout << "Developed By: Rich Hoggan" << endl;
    cout << endl << endl;
    cout << "Usage Statement:" << endl;
    cout << "The following command definitions are allowed in the program:" << endl;
    cout << "QuickSandMain.cpp <specimen file name>" << endl;
    cout << "\t<specimen file name> in this case is a .txt file containing the" << endl;
    cout << "\tobfuscated specimen file." << endl;
    cout << endl;
}

void printErrorMessage(string errorTypeFlag)
{
    if (errorTypeFlag == "INVALID_FILE_TYPE")
    {
        cout << "Unable to continue, it looks like an error occurred." << endl;
        cout << "The specimen file you provided was not readable.  Please" << endl;
        cout << "double check the file name and try again." << endl;
    }
}