//Include Directives
#include <vector>
#include <string>
#include <iostream>
using namespace std;

//Function Name: tokenizeSpecimenFileContents()
//Function Description: Tokenizes the specimen file's contents and returns a vector of vector strings
//containing line tokens.
vector<vector<string>> tokenizeSpecimenFileContents(vector<string> specimenFileContents)
{
    //Variable declarations
    int specimenFileContentsLength = specimenFileContents.size();
    string currentToken = "";
    int currentPosition = 0;

    vector< vector<string> > parsedSpecimenFileContents;

    //Determine if the incoming specimenFileContents data structure
    //contains content
    if (specimenFileContentsLength > 0)
    {
        //Begin iterating through the data structure
        for (int currentLineIndex = 0; currentLineIndex < specimenFileContentsLength;
             currentLineIndex++)
        {
            //Get the current line from the file contents and begin parsing
            string currentLine = specimenFileContents[currentLineIndex];
            vector<string> currentTokenLine;
            for (int currentCharacterIndex = 0;
                 currentCharacterIndex < currentLine.length(); currentCharacterIndex++)
            {
                //Get the current character from the current line and
                //determine if the current index is a , and continue to next iteration
                char currentCharacter = currentLine.at(currentCharacterIndex);
                if (currentCharacter == ',') { continue; }

                if (!isspace(currentCharacter))
                {
                    //Concat the current character with the currentToken variable
                    currentToken = currentToken + currentCharacter;

                    //Determine if the current index position is equal to the ending index position
                    if ((currentCharacterIndex + 1) == currentLine.length())
                    {
                        currentTokenLine.push_back(currentToken);
                        currentToken = "";
                    }
                }
                else if (isspace(currentCharacter))
                {
                    currentTokenLine.push_back(currentToken);
                    currentToken = "";
                }
            }

            //Append the completed token line to the parsedSpecimenFileContents data structure
            parsedSpecimenFileContents.push_back(currentTokenLine);
        }
    }

    //Return parsedSpecimenFileContents on completion
    return parsedSpecimenFileContents;
}

vector<vector<string>> parseAsStack(vector<vector<string>> tokenizedSpecimenFileContents,
    string parsingType)
{
    //Variable declarations
    int tokenizedSpecimenFileContentsLength = 0;
    vector<vector<string>> parsedStack;

    //Validate function arguments before continuing
    tokenizedSpecimenFileContentsLength = tokenizedSpecimenFileContents.size();
    if (tokenizedSpecimenFileContentsLength > 0 && parsingType == "PUSH_STACK"
            || parsingType == "INST_STACK")
    {
        //Iterate through the tokenized file contents and parse on push opcode
        for (int currentLinePosition = 0; currentLinePosition < tokenizedSpecimenFileContents.size();
             currentLinePosition++)
        {
            //Get the current line and parse out the opcode
            vector<string> currentLine = tokenizedSpecimenFileContents[currentLinePosition];
            string currentOpCode = currentLine[0];

            //Determine which type of stack to parse on
            //PUSH_STACK -> only parse on push instruction opcode
            //INST_STACK -> only parse on all non-push instruction opcodes
            if (parsingType == "PUSH_STACK" && currentOpCode == "push")
            {
                parsedStack.push_back(currentLine);
            }
            else if (parsingType == "INST_STACK" && currentOpCode != "push")
            {
                parsedStack.push_back(currentLine);
            }
        }
    }

    //Return parsedStack on completion
    return parsedStack;
}