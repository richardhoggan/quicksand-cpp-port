//
// Created by Richard Hoggan on 4/16/17.
//

#ifndef QUICKSAND_EMULATIONOUTPUT_H
#define QUICKSAND_EMULATIONOUTPUT_H

#include <string>
#include <vector>
#include <map>
using namespace std;

class EmulationOutput
{
public:
    map<string, unsigned int> emulatedMainMemoryMap;
    map<string, unsigned int> sandboxedEmulatedMainMemoryMap;
};

#endif //QUICKSAND_EMULATIONOUTPUT_H
